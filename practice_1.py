#    Суть алгоритма. 
#    Методом solver выполняю препроцессинг - выполняю проверку типов и
#    убираю лишние пробелы из входа, далее получаю final_set вызывая
#    find_set. Если final_set будет лежать слово symbol * k, где symbol
#    и k - символ и число из условий задачи, то в языке есть слово с 
#    необходимым префиксом и ответ на задача True, иначе возвращаю False.  
#    Методом find_set я последовательно прохожусь по регулярке и на каждом шаге поддерживаю
#    множество префиксов длины k слов, которые задаются разобранным к 
#    данному шагу регулярным сообщением. В конце возвращаю итоговое 
#    множество.
#
#    Подробнее об операциях.
#    Конкатенация. Метод concatenation. Из декартова произведения двух предыдущих множеств в 
#    новое множество я добавляю только префиксы длины k тех слов, которые
#    начинаются на symbol.
#    Звезда Клини. Метод star.Если в множестве есть элемент состоящий только из symbol, 
#    то зациклив его я получу необходимый префикс, поэтому добавляю в новое 
#    множество symbol * k. Слова содержащие другие символы нас не интересуют,
#    потому что их повторение не поможет нам получить более длинный префикс 
#    из  symbol.
#    Сумма (или). Метод plus. Объединяю два данных множества в одно новое. 
 
ALPHABET = "abc1"

def solver(string, symbol, k):
    try:
        k = int(k)
        if ((not isinstance(string, str)) or (not symbol in ALPHABET) or (not isinstance(k, int))):
            raise Exception("Wrong input")
    except: 
        raise Exception("Wrong input")

    if k == 0:
        return True

    string = string.replace(" ", "")
    final_set = find_set(string, symbol, k)
    for element in final_set:
        if element[:k] == symbol * k:
            return True
    return False


def concatenation(f_set, s_set, symbol, k):
    result = set()
    for f_element in f_set:
        for s_element in s_set:
            if f_element and f_element[0] == symbol:
                result.add((f_element + s_element)[:k])
            elif f_element == "" and s_element and s_element[0] == symbol:
                result.add((f_element + s_element)[:k])
    return result

def star(current, symbol, k):
    new_set = {""}
    for element in current:
        if set(element) == {symbol}:
            new_set.add(symbol * k)
    return current | new_set

def plus(f_set, s_set):
    return f_set | s_set

def find_set(string, symbol, k):
    stack = list()
    for element in string:
        try:
            if element in ALPHABET:
                stack.append({element})
            else:
                if element == '*':
                    stack.append(star(stack.pop(), symbol, k))
                else:
                    s_set = stack.pop()
                    f_set = stack.pop()
                    if element == '+':
                        stack.append(plus(f_set, s_set))
                    else:
                        a = concatenation(f_set, s_set, symbol, k)
                        stack.append(a)
        except IndexError:
            raise Exception("Wrong regular expression")

    if len(stack) != 1:
        raise Exception("Wrong regular expression")
    return stack[0]

def main():
    if solver(input(), input(), input()):
        print("YES")
    else:
        print("NO")

if __name__ == '__main__':
    main()