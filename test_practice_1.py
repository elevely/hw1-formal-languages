from practice_1 import *

def test_true_one():
    assert solver("ab + c.aba. * .bac. + . + *", "a", "2") == True


def test_true_two():
    assert solver("acb..bab.c. * .ab.ba. + . + *a.", "b", "3") == False


def test_true_three():
    assert solver("bab. + ab + . a *.", "b", 2) == True


def test_false_one():
    assert solver("bab. +  ab + . a *.", "b", 3) == False
